## je prends des notes sur le cours sur les graphiques

library(tidyverse)

df <- read_tsv("Data/plotme.tsv")

## calculer la moyenne, la dev st, de x et y par "dataset"

df %>% 
  group_by(dataset) %>% 
  summarise(mean_x = mean(x), mean_y = mean(y),
            devst_x = sd(x), devst_y = sd(y))

# plot
df %>% 
  ggplot()+
  aes(x, y)+
  geom_point()+
  facet_wrap(~dataset)


## on attaque avec ggplot2


## un jeu de données pour faire nos essais
mpg


## un plot commence toujours avec ggplot

ggplot(mpg)

## opératuer "+"

firstplot <- ggplot(mpg)+
  aes(x = cty, y = hwy)

firstplot

firstplot +
  geom_point()

firstplot+
  geom_smooth()

firstplot+
  geom_point()+
  geom_smooth()

## ajouter d'autres couches

firstplot+
  geom_point(aes(color = displ))

firstplot+
  geom_point(aes(size = displ, color = class, shape = fl))

## faceting

firstplot+
  geom_point()+
  facet_wrap(~class)



library(nycflights13)

## exo1
## plottez la relation entre dep_delay et arr_delay
## pour le mois de JANVIER
## faites un sublot pour chaque aéroport
## coloriez par aéroport d'origine
flights %>% 
  filter(month == 1) %>% 
  ggplot() +
  aes(x = dep_delay, y = arr_delay, color = origin)+
  geom_point()+
  facet_wrap(~origin)

## exo2
## calculez le delai moyen au depart par origine et destination
## sauvegardez ça dans le jeu de donnes "exo2"
##
## plottez ce delai moyen par origine et destination

exo2 <- flights %>% 
  group_by(origin, dest) %>% 
  summarise(mean_delay = mean(dep_delay, na.rm = T))

exo2 %>% 
  ggplot() +
  aes(y = dest, x = mean_delay)+
  geom_point()+
  facet_wrap(~origin)

## galerie de possibilités

## plot d'une variable

## var discrète

p <- ggplot(mpg)+aes(x = drv)

p + geom_bar()

## on peut jouer sur d'autres éléments géometriques
## couleur
## remplissage

## si on utilise AES()
p + geom_bar(aes(fill = drv))

## si on utilisie PAS AES()
p + geom_bar(fill = "pink")

## on peut mapper la couleur sur une AUTRE variable

p + geom_bar(aes(fill = class))


## une variable, continue

p2 <- ggplot(mpg)+aes(x = hwy)

## histogram
p2 + geom_histogram()


## manipuler le nombre de bins
p2 + geom_histogram(bins = 1)
p2 + geom_histogram(bins = 2)
p2 + geom_histogram(bins = 3)
p2 + geom_histogram(bins = 4)
p2 + geom_histogram(bins = 5)

p2 + geom_histogram() +
  facet_wrap(~drv)


## densité
p2 + geom_density()

## manipuler le kernel density estimation
p2 + geom_density(adjust = .1)
p2 + geom_density(adjust = 3)

## on peut utiliser tout le reste

p2 + geom_density(aes(color = drv, fill = drv))+
  facet_wrap(~drv)

## DEUX variables

## deux continues

## voir ci-dessus

p3 <- ggplot(mpg)+aes(x = cty, y = hwy)

# scatter
p3 + geom_point()

# smooth
p3 + geom_smooth()

# par défaut smooth non-linéaire
# si on veut un smooth linéaire
p3 + geom_smooth(method = "lm")+geom_point()
  
  
# comme d'hab on peut tout changer et assigner + de variables
p3 + geom_smooth(aes(color = drv), method = "lm")
  
  
## une variable discrète et une continue
p4 <- ggplot(mpg) + aes(x=drv, y = hwy)
p4

## on peut faire un scatter? 
p4 + geom_point()

## on peut faire des boxplot

p4 + geom_boxplot()

## on peut faire des violons

p4 + geom_violin() ## comme un geom_density mais par niveaux


## on peut faire des barres!
## geom_col() > comme geom_bar() mais avec DEUX variables

mpg %>% 
  group_by(drv) %>% 
  tally() %>% 
  ggplot() +
  aes(drv, n) +
  geom_col()

## deux variables catégorielles
mpg %>% 
  ggplot()+
  aes(drv, class)+
  geom_count()

## trois variables

## geom_tile()

ggplot(mpg)+
  aes(x = cyl, y = drv, fill = hwy)+
  geom_tile()





