---
title: "Exercices L4"
format: html
editor: visual
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(nycflights13)
```

# Let's revise

> We are going to run exercises on the `babynames` package, so let's start by installing and exploring it. What does it contain?

```{r}
# install the babynames package
# install.packages("babynames")

# call the babynames library
library(babynames)

# inspect the babynames dataset
babynames

# assigner à df
df <- babynames

summary(df)
```

## Exercice 1: wrangling

> Using `babynames`, compute the average proportion of "Mary" and "John" by decade

```{r ex1}
# attention: il n'y a pas de variable pour la décennie, faut la créer d'abord!
# floor() prend la partie entiere de la division inf
# celling() prend la partie entiere mais sup

exo1 <- df %>% 
  mutate(decade = floor(year/10)) %>% 
  group_by(decade, name) %>% 
  filter(name == "John" | name == "Mary") %>% 
  summarise(proportion = mean(prop))

```

## Exercice 2: plotting

> Using `babynames`, plot the proportion of "Mary" and "John" over the years as a line

```{r ex2}
## solution en utilisant les décennies
exo1 %>% 
  ggplot()+
  aes(x = decade, y = proportion, color = name)+
  geom_line()

## solution par année
df %>% 
  filter(name == "John" | name == "Mary") %>% 
  ggplot()+
  aes(x = year, y = prop, color = name)+
  geom_line()+
  facet_wrap(~sex)

  
```

## Exercice 3: wrangling + plotting

> Using `babynames`, plot the proportion in time for the three most used names in 1880, separately for males and females (seems simple, but there is a catch, you'll see!)

```{r ex3}



```

# Joining

## Exercice 1

> using `nycflights13`, count how many flights through NYC land in an airport whose altitude is \> 1000mt

```{r ex4}
# note: 1 mètre = 3,28084 feet
# altitude is in the airports df (in feet)
# flights are in the flights df


```

## Exercice 2

> using `nycflights13`, compute how old are on average the planes that fly to airports whose altitude is \>1000mt, as opposed to \>1000mt?

```{r ex5}
# need data from three datasets!
# remember: you join always two by two, so you need two joins





```

## Exercice 3

> using `nycflights13`, compute how many potential passangers arrive in each NYC airport in one year

```{r ex6}
# note: in the planes dataset you get the numer of seats of each plane
# note: combine that info with flights, then do some summarise()...
```

# Reshaping

> we will use the data from the penguins on three different islands in the South Atlantic. The data are to be found in the `palmerpenguins` package.

## Exercice 1: setup

> install and load `palmerpenguins`, then explore the `penguins` dataset

```{r ex7bis}
# install palmerpenguins
# import penguins as df
# inspect the df: how many penguns species? how many islands?

```

## Exercice 2: `pivot_wider()`

> compute the average body mass by penguin species and by island. Then use pivot_wider() to get a nice table with species on the rows and islands on the columns, mean body mass in the cells.

```{r ex8}

  
```

## Exercice 3: `pivot_longer()`

> run the cell to create the `df` for this question.

```{r ex8bis}

df <- tribble( ~exam, ~john, ~mary, ~charles, ~anna,
               "R", 12, 14, 18, 8,
               "Psy", 4, 19, 11, 13,
               "Econ", 12, 9, 8, 17,
               "Maths", 15, 11, 11, 12)

```

> now, make this df tidy: create a variable `student` and a variable `mark` for each exam.

# Tidying

## Exercice 1: `separate()`

> using `babynames`, separate the `year` variable into `millenium`, `century`, `decade`

```{r ex10}

  
```

## Exercice 2: `unite()`

> `unite()` the three variables created above into `year`.

```{r ex11}

  
```
