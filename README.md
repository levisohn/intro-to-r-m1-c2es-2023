# Logiciels spécialisés: R

## Manipulation et présentation des données avec R

*Paolo Crosetto - 24 heures -- Septembre/Novembre 2023*

Dans ce cours, on va utiliser le logiciel R, et en particulier son 'dialect' le `tidyverse` pour

-   manipuler
-   visualiser
-   analyser des données, et
-   produire des rapports avec les résultats de l'analyse.

### Pré-requis

Aucun, mais une quelque familiarité avec la statistique et les données est bienvenue.

### Pré-requis techniques

-   les étudiants sont priés de venir en cours *avec leurs PC portables*
-   (si pas possible, on va pouvoir utiliser les PC portables de l'université, mais il y en a peux)
-   installation de R (gratuit, ici: <https://pbil.univ-lyon1.fr/CRAN/> )
-   installation de Rstudio (gratuit, ici: <https://www.rstudio.com/products/rstudio/download/#download> )
-   installation du package `tidyverse`:
    -   ouvrez Rstudio
    -   assurez-vous que votre PC soit connecté à internet
    -   dans la console (en bas à droite) tapez `install.pacages(“tidyverse”)`
    -   allez boire un café (cela prend quelques minutes)
-   installation de `git` (gratuit, ici: <https://git-scm.com/downloads> )
-   activation du compte `gitlab` UGA (ici: <https://gricad-gitlab.univ-grenoble-alpes.fr/>)

### Structure du cours

1.  **prise en main du logiciel**: `R`, Rstudio, pourquoi `R` et non pas un autre logiciel, ressources (gratuites) en ligne pour apprendre (livres, sites, twitter, blogs, wikis...).
2.  **workflow**: travailler de façon efficace, seuls ou à plusieurs ; documenter son code ; utiliser des repository ouverts pour gérer le code et l'intéraction avec des coauteurs ou le public. Usage de base de `git` et de `gitlab`.
3.  **plotting**: dire la vérité et mentir avec les données, bad and good plots; `ggplot2`, the grammar of graphics; #tidytuesday (package de référence : `ggplot2`).
4.  **manipuler les données**: sélectionner, filtrer, transformer, nettoyer, reshape, merge, création de variables, données 'propres' et 'ordonnées', tidy data (package de référence : `dplyr` et `tidyr`).
5.  **analyser les données**: statistiques descriptives, analyse par groupe, modèles simples de régression ; appliquer une analyse de façon récursive, pour chaque groupe ; comparer les résultas (package de référence : `dplyr`, `broom`).
6.  **web crawling** : extraire des données de sites web (package de référence : `rvest`, `RCrawler`)
7.  **créer des rapports de stat**: rapports dynamiques avec `Rmarkdown`, `knitr` and `quarto`; mise à jour des rapports (package de référence : `knitr`, `quarto`).

### Contrôle final

La note pour ce cours est obtenue avec deux épreuves: 

1. En rédigeant un **rapport statistique** et en le présentant en classe le 6 novembre;
2. En analysant des nouvelles données en classe, dans une séance d'examen le 15 novembre.

#### Rapport de stats + présentation

Les étudiants auront **un mois** (du 4 octobre au 6 novembre) pour produire un rapport statistique sur des données qu'ils auront choisi (une liste de jeux de données va être fournite à l'avance). Le rapport sera rédigé en `quarto` et téléversé sur la page `gitlab` de chauqe étudiant. Le développement se fera également sur `gitlab` et les codes sources seront ouverts et accessibles. La note, en /20, sera obtenue en présentant son travail aux collègues et en répondant à des questions sur ce même travail lors de la présentation. 
Présentation + rapport de stats vaudront pour 2/3 (66%) de la note finale.

La préssentation aura lieu pendant le 8ème et dernière séance du cours, le lundi 6 novembre, 9h-12h. 

Exemple d'un produit final possible: <https://jtanwk.github.io/us-solar/#how_have_solar_panel_costs_changed>

Exemple d'un produit final fait par une étudiante de 2020: dans ce même repo, sous `/Exam/Exemple_2022`.


#### Examen en classe

La deuxième partie de la note sera l'objet d'une analyse de données nouvelles (i.e. que les étudiants n'auront pas vu auparavant) en classe. Vous aurez 2 heures, vous pourrez choisir parmi 3 jeux de données, et vous devrez répondre à une série de questions sur les données que vous aurez choisies. La note, en /20, dépendra de vos réponses et de votre code. 
Cette partie vaudra pour 1/3 (33%) de la note finale. 

L'analyse de données en classe aura lieu le mercredi 15 novembre, de 14h à 16h. 




