---
title: "Introduction to R and the tidyverse"
subtitle: "-- getting started --"
author: "Paolo Crosetto"
format:
  revealjs: 
    theme: dark
---

# Know your teacher

## The teacher

It's `Paolo`:

-   a researcher at INRAE
-   experimental economics, food consumption, risk
-   passionate about econ, stats & R
-   teaching also Experimental Methods to M2 EEED
-   [paolo.crosetto\@inrae.fr](paolo.crosetto@inrae.fr) or [\@gmail.com](paolo.crosetto@gmail.com)
-   Office 413, 4th floor, BATEG (seldom there)

# Know your course

## Language

Slides will be in **English**.

-   **All** R documentation is in English.

-   Out there you will be needing help.

-   Help with mostly be in English.

> `Best get used to it`.

## The course

This course will be a *24 hour* introduction to R, using the *tidyverse*, a set of packages[^1] developed by Hadley Wickham & people at Posit (formerly Rstudio).

[^1]: We'll know what that means later today

-   data `input` & `output`
-   data `wrangling`, selecting; filtering, mutating
-   data `reshaping` and `merging`
-   data `plotting`
-   web crawling & `scraping`

## Our take on R: in general

-   **`R` is a programming language for statistics**.

-   It can do **many** things:

    -   these *slides*

    -   *websites*

    -   *simulations*

    -   ...

-   here we will focus on `descriptive data analysis`

## Our take on R: in practice

> In practice, we will deal with `data.frame`s

> We will manipulate, extract, analyze, plot, more or less complex `data.frame`s

> We will **not** do high-level `R` programming or low-level `R` commands.

# Schedules & lectures

## Lecture plan

-   **L1** (6/9): setup, `tools`, workflow
-   **L2** (7/9): data IO & basic data `wrangling`
-   **L3** (13/9): `advanced` data `wrangling`
-   **L4** (14/9): basic `plotting`
-   **L5** (22/9): `advanced` plotting
-   **L6** (27/9): tidy statistical `analysis`
-   **L7** (4/10): `scraping` data from the web
-   **L8** (6/**11**): `presentation` of your **stats report**

## Material

All slides are written in `R` itself, using `quarto`[^2].

[^2]: We'll know what that means later today

For each lecture, you have (or will have):

-   a `.qmd` file (source)
-   the rendered `.html`
-   a `notes_L[x]` file by me in `/Paolos Notes`
-   an `exer_L[x]` file in `/Exercises`

All slides are available at the [gitlab](https://gricad-gitlab.univ-grenoble-alpes.fr/crosettp/uga_ma_intro_r_2023) repository of the course

## How lectures work

### Me:

-   there are **slides** (in `/Lecture X`)
-   I take notes in a **notes file** (in `/Paolos_Notes`)
-   I will provide exercises (in `/Exercises`)

::: {.fragment}

### You:

-   you have a **personal folder** `/Student_Area/SURNAME`
-   you take notes, make changes... **exclusively** there
-   at home, copy exercises in your folder and solve them
:::

# Exam

## Exam

The exam is composed of two parts

::: {.fragment}

1.  You write a **statistical report** that you `present` in class. This is worth 2/3 of the final mark.
:::

::: {.fragment}

2.  You **analyze** fresh data in a dedicated `exam session`. This is worth 2/3 of the final mark.

:::

## Exam/1: report

You will have `a month` (Oct 4th - Nov 6th) to prepare a **statistical report** on a topic of choice.

::: {.fragment}

-   done within a `quarto .qmd` file
-   hosted on the `gitlab` page of our course
-   a mixture of `text`, `code`, `results` and `plots`

:::

::: {.fragment}

Example: [here](https://jtanwk.github.io/us-solar/#how_have_solar_panel_costs_changed)

:::

## Exam/2: presentation

You will `present` the work to the class on `Nov 6th`.

::: {.fragment}

-   5 minutes (sharp!) presentation
-   1 minute Q&A with questions from me
-   no need to prepare slides (present your report)

:::

## Exam/3: live data analysis

On `Nov 15th` we will spend 2 hours analyzing data.

::: {.fragment}

-   Three `fresh datasets` (that you have never seen before)
-   Choose one!
-   For each, `some questions` to be answered
-   You create a .qmd data analysis `on the fly`
-   and push it to your gitlab when done.

:::

# Questions? 

# Ready? Go!
