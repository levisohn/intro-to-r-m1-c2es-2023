#Notes du cours 2 
library(tidyverse)
df <- read_tsv("Data/plotme.tsv")

#{calculer la moyenn, la sd, de x et d e y }

df %>% 
  group_by(dataset) %>% 
  summarise(mean_x=mean(x), 
            mean_y=mean(y),
            sd_x=sd(x),
            sd(y))

#ggplot2 
#on commence par un jeunde données mpg pour faire nos essais 

mpg

first_plot <- ggplot(mpg)+
  aes(x=cty, y=hwy)

#ajouter des points 
first_plot<- first_plot+geom_point() #ajouter des lignes
first_plot
#ajouter d'autres lignes 
plot_1 <- ggplot(mpg)+
  aes(x=cty, y=hwy)+
  geom_point()+
  geom_point(aes(color=displ,size=cyl)) #on peut ajouter des variables dans les rubriques
#geom point pour definir comment on veut les nouvelles varibales sur le graphe
plot_1

#facets (afficher plusieurs graphes sur un seul graphe)
first_plot+ 
  facet_wrap(~class)

#exo1
library(nycflights13)
#ploter la relation entre dep_delay, arr_delay pour janvier 


df_1 <- flights %>% 
  filter(month==1) %>% 
  ggplot()+
  aes(x=dep_delay, y=arr_delay, color=origin)+
  geom_point()+
  facet_wrap(~origin) #pour chaque aeroport 

#exo2 
#calculez le délai moyen au départ et par origine de destination
#plottez le délai moyen par origini et destination

exo_2 <- flights %>% 
  group_by(origin, dest) %>% 
  summarise(delai_moy=mean(dep_delay, na.rm=T)) %>% 
  ggplot()+
  aes(x=delai_moy, y=dest)+
  geom_point()+
  facet_wrap(~origin)
exo_2

#gall&rie de possibilités 

#variable discrète 

plot_2 <- ggplot(mpg)+
  aes(x=drv,fill=drv)+
  geom_bar()
#si on veut mapper la couleur sur une autre variable 
plot_3 <- ggplot(mpg)+
  aes(x=drv,fill=class)+
  geom_bar()


#variable continue 
plot_4 <- ggplot(mpg)+
  aes(x=hwy)

plot_4 + geom_histogram(aes(fill=drv)) #histogramme    #on peut manipuler le nombre de bins en utilisant le paramètre "bins" entre ()
plot_4 + geom_density(aes(fill=drv)) #densité

#Deux variables 

#deux variables continues 
#voir ci dessus 

plot_5 <- ggplot(mpg)+
  aes(x=drv, y=hwy)
  
  #geom_point() nuage de points

#smooth
plot_5 + geom_smooth(method='lm')+geom_point() #method lm pour régression  lineaire 

#une variable discrète et une variable continue 

#boxplot
plot_5+geom_boxplot()
#violon
plot_5+geom_violin()

#on peut faire des barres 
mpg %>% 
  group_by(drv) %>% 
  tally() %>% 
  ggplot()+
  aes(x=drv, y=n)+
  geom_col()

#variables catégorielles 

mpg %>% 
  ggplot() +
  aes(x=drv, y=class)+
  geom_count(aes(color=class))


#3 variables 

ggplot(mpg)+
  aes(x=trans , y=manufacturer, fill=hwy)+
  geom_tile()










