#Ploting
#ploter des donnees de facon plus simple
library(tidyverse)
df<-read_tsv("Data/plotme.tsv", col_names = TRUE)

#dataset groupe by dataset, et pour chacun de ce groupe tu va calculer
df%>%
  group_by(dataset) %>%
  summarise(mean_x = mean(x), mean_y = mean(y),
            devst_x = sd(x), devst_y = sd(y))
#plot
df %>%
  ggplot()+
  aes(x,y)+
  geom_point()+
  facet_wrap(~dataset)

#c'est bien de faire une bonne , graphique - bcp d'information dans petit espace
#bon graphique, paser l'information, c'est super facile de mentir avec graphique, c'est tres
#important d'etre honnete, les proportions doivent correspondre aux chiffres
#il faut bien connaitre des donnes et il faut reflechir, c'est bon de sketchs



#un jeu de donnees
mpg

#un plot commence avec ggplot
ggplot(mpg)
# operateur + pour ajouter des elements

firstplot <- ggplot(mpg)+
  aes(x = cty, y = hwy)

firstplot +  geom_point(colour = 'red')

firstplot +  
  geom_smooth() +
  geom_point()

#ajouter d'autres couches

firstplot +
  geom_point(aes(color = trans))  #colour selon la classe

firstplot +
  geom_point(aes(color = class, size = displ, shape = fl))  #on a mis 4 variables
#c'est pas une bonne idee de metrre tous dans un seul graphique

#facets - graph pour chacun des elements

firstplot +
  geom_point()+
  facet_wrap(~class)
#ex01
library(nycflights13)

flights %>%
  filter(month == 1) %>%
  ggplot() +
      aes(x = dep_delay, y = arr_delay, color = origin) +
      geom_point() #+
      #facet_wrap(~origin)
  
##exo2
## calculez le delai moyen au depart par origine et distination
## sauvgardez ca dans le jeu de donnees "exo2"
## plotez ce delai moyen par origine et destination

exo2 <- flights %>%
  group_by(origin, dest) %>% 
  summarise(mean_delay = mean(dep_delay, na.rm = T)) %>% 
  arrange(mean_delay)

exo2 %>%
  ggplot()+
  aes(y = dest, x = mean_delay)+
  geom_point() +
  facet_wrap(~origin)


## galerie de possibilites
#si vous aviez donnees catigorielle est continu


## plot d'une variable
## var discrete
p<- ggplot(mpg) + aes(x=drv)

p + geom_bar() 
#on peut jouer sur d'autres elements geometriques
#couleur et remplissage

#si on utilise AES()
p + geom_bar(aes(fill = drv)) #kolory rozdzielone, ils depandent de variable

#si on n'utilise pas AES()
p+geom_bar(fill = 'pink')

#on peut mapper la couleur sur une autre variable

p + geom_bar(aes(fill = class)) # ici 3 variables

#une variable continue

p2 <- ggplot(mpg) + aes(x=hwy)

## hitogram
p2 + geom_histogram() #on a cree des bins/panier et on a compte combien des observations
#manipuler le nombre de bins
p2 + geom_histogram(bins = 25) +
  facet_wrap(~drv)


## densite
p2 + geom_density() #aproximation continu de histogram

## manipuler le kernel density estimation
p2 + geom_density(adjust = .3)
p2 + geom_density(adjust = 5) #plus proche au normal

## on peut utiliser tous le reste
p2 + geom_density(aes(color = drv, fill = drv)) +
  facet_wrap(~drv)

## DEUX variables
p3 <- ggplot(mpg) + aes(x = cty, y = hwy)
#scatter
p3 + geom_point()

p3 + geom_smooth(method = "lm") + geom_point()

#comme d'hab on peut tout changer et assigner + de variables
p3 + geom_smooth(aes(color = drv), method = "lm")

#une variable discrete et une continue
p4 <- ggplot(mpg) + aes(x=drv, y = hwy)
p4
## scatter? pas vraiment
p4 + geom_point()

#on peut faire des boxplot
p4 + geom_boxplot()

#on peut faire des violons
p4 + geom_violin()  #comme un geom_density mais par niveaux

#on peut fasire des barres>????????????
## geom_col() > comme geom_bar() mais avec deux variables

mpg %>% 
  group_by(drv) %>%
  tally() %>%
  ggplot() +
  aes(drv, n)+
  geom_col() 

# deux varuables categorielles

mpg %>%
  ggplot()+
  aes(drv, class)+
  geom_count()
## trois variables

ggplot(mpg)+
  aes(x = trans, y = drv, fill = hwy) +
  geom_tile()

#ex1
flights %>%
  ggplot()+
  aes(x = arr_delay)+
  geom_density()
