library(tidyverse)


## commentaire

## executer une muktiplicatio on execute avec ctrl+entée 

12*12

## creer un vecteur 

c(1, 2, 3, 4, 5) ## c veut dire "concatenate"

c("ann","bob","cat","door")

c(1,"ann")

## 2 assignation <- 

vecteur1 <- c(1,2,3,4)

c(1,2,3,4,5) -> vecteur_dans_lautre_sens

vecteur_de_noms <- c("paolo","abdel","julia","karine")

## 3 matrice 

vec1 <- c(1,2,3,5)
vec2 <- c(4,5,6,6)

## matrice par ligne 

rbind(vec1,vec2) -> matrix2

## matrice par colonne 

cbind(vec1,vec2) -> matrix1

# extraire cdonnées de matrices 

## element 3 de la ligne 1
matrix1[1,3]


### Jeux de Données : 

library(tidyverse)

##assigner un jeu de données à un objet 
## nous permet de l'observer dqns un environnement 
##et de l'explorer 

df <- mpg

## pour extraire une var utiliser "$"
mpg$year

## travailler sur le TYPE de varibale 

## types connus jusqu'ici 

# "chr" chaine de caractere 
# "dbl" c'est un num réel 
# "int" numeros entier 

## check the type of variable 
##  use "is" function 

is.character(mpg$year)
is.integer(mpg$year)

## set the type of variable 
## us "as" function 

# change year into real number 
as.double(mpg$year)

# change year into charactere 
as.character(mpg$year)

# Exercice 1 
vec_1 <- c(15,24,34,6)
vec_2 <- c(14,28,68,84)
matrix3 <- cbind(vec_1,vec_2)
