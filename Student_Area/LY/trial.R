#### on essai
1+1
2+2
3*5
12*12
## créer un vecteur
vec1 <- c(1,3,6)
vec2 <- c(7,2,5)
## matrice
rbind(vec1,vec2) -> matrix1
cbind(vec1,vec2) -> matrix2
matrix1[1,3]
### jeux de données
library(tidyverse)
df <- mpg
## extraire une variable
mpg$year -> mpg_year_extrait

# "chr" chaine de caractères
# "dbl" c'est un numéro réel
# "int" numéro entier

## check the type of a variable
## use "is" function

is.character(mpg$year)
is.integer(mpg$year)

## set the type of a variable
## use the "as" function

# change year into real number
as.double(mpg$year)

# change year into character
as.character(mpg$year)

## exercice 1
vec1 <- c(12, 24, 36, 48)
vec2 <- c(9, 7, 5, 3)
matrix3 <- cbind(vec1, vec2)



## installer un package qui contient des données
install.packages("nycflights13")

## quand on veut utiliser un package
## il faut l'appeler
## en utilisant "library"
library(tidyverse)
library(nycflights13)

## importer les données dans l'espace de travail

df <- flights

## voir les données dans la console
df

## voir les données dans Rstudio
View(df) ## ou bien vous cliwuez sur le jeu de données dans "Environment"

## fonction sommaire des données

summary(df)

## autre fonction pour prise en main jeu données
install.packages("skimr")
library(skimr)
skim(df)

#### data manipulation

## filter

# tous les vols de janvier
filter(df, month == 1)

# on peut fair eplusieurs filtres à la fois
# les vols du 12 janvier
filter(df, month == 1 & day == 12)

# on peut utiliser de la logique négative
# les vols qui ne sont PAS partis de JFK
filter(df, origin != "JFK")

## multiple statemets avec %in%

## vols du printemps
## printemps = mars, avril, mai
printemps <- filter(df, month == 3 | month == 4 | month == 5)

filter(df, month %in% c(3, 4, 5))


## mise en ordre des données

## arrange()

# ordre croissant
arrange(df, dep_delay)

# ordre décroissant
arrange(df, -dep_delay)


## sélectionner des colonnes: select()

# df avec date, lieu de départ, lieu d'arrivée
select(df, year, month, day, origin, dest)

# df sans tailnum
select(df, -tailnum)

# df sans quelques variables
select(df, -tailnum, -month, -year, -dest)

# using starts_with
# toutes les variables relatives au "delay"
names(df)

select(df, ends_with("delay"))

# toutes les variable relative au départ
select(df, starts_with("dep"))

# contains()
# toutes les var relatives au départ
select(df, contains("dep"))

## utiliser tout ce qu'on a dit
select(df, month, year, day, contains("dep"))

## rename
## changer le nom des variables
## passer tout en français

rename(df, mois = month, 
       annee = year, 
       jour = day)

### exo1

### créez un jeu de données qui s'appelle "noel"
### et qui contient tous les vols du 25 décembre
### avec variables dep_time et dep_delay


##
noel <- filter(df, month == 12 & day == 25)
noel <- select(noel, starts_with("dep"))



### dites bonjour au "pipe" %>%
filter(df, month == 12 & day == 25)

## deux codes équivalents


noel <- df %>% 
  filter(month == 12 & day == 25) %>% 
  select(starts_with("dep"))

## exemple avec des maths

2*2

exp(2*2)

(2*2) %>% 
  exp()

## exo2

## en utilisant le pipe, 
## créez un jeu de données
## qui contient tous les vols entre JFK et LAX
## partis en mars
## et gardez le jour et le retard au départ
## ordonnez-les par retard décroissant 
## et mettez les noms en bon français

exo2 <- df %>% 
  filter(origin == "JFK" & dest == "LAX") %>% 
  filter(month == 3) %>% 
  select(day, dep_delay) %>% 
  arrange(-dep_delay) %>% 
  rename(jour = day, retard = dep_delay)
exo2


### changing data (yay!)

## create new variables or change existing ones
## with mutate()

## create an index starting at 1 for 2013
df %>% 
  mutate(starting_year = year - 2012)

## calculer la vitesse des avions

## distance parcourue est "distance" (in MILES)
## temps en l'air est "air_time" (in MINUTES)

df %>% 
  mutate(speed_basic = distance/air_time) %>% 
  select(distance, air_time, speed_basic)

## on peut utiliser les nouvelles variables crées TOUT DE SUITE

## créer la vitesse et la vitesse au carré

# avec deux calls to mutate
df %>% 
  mutate(speed_basic = distance/air_time) %>% 
  mutate(speed2 = speed_basic^2)%>% 
  select(distance, air_time, speed_basic, speed2) ## <- ceci sert juste à vous montrer le résultat

# avec le même call
df %>% 
  mutate(speed_basic = distance/air_time, speed2 = speed_basic^2) %>% 
  select(distance, air_time, speed_basic, speed2)

# create a logical variable

# une variable qui me dit si l'aeroport est JFK
df %>% 
  mutate(is_JFK = origin == "JFK") %>% 
  select(origin, is_JFK)

## attention!

## si mutate est assigné à une nouvelle variable, tout va bien
#" si mutate est assigné à la MEME variable, on l'écrase

df %>% 
  mutate(origin2 = "paolo") %>% 
  select(origin, origin2)



## summarise pour faire des sommaire de variable

## une moyenne!

## quel est le delay moyen au départ et à l'arrivée? 
df %>% 
  summarise(mean_dep_delay = mean(dep_delay, na.rm = TRUE), 
            mean_arr_delay = mean(arr_delay, na.rm = TRUE))


## on peut faire des sommaires groupés en utlisant
## group_by()

## quel est le retard moyen à l'arrivée
## pour chaque mois? 
df %>% 
  group_by(month) %>% 
  summarise(mean_arr_delay = mean(arr_delay, na.rm = TRUE))


## on peut grouper par plus qu'une variable
## retard par mois et par aeroport de départ
df %>% 
  group_by(month, origin) %>% 
  summarise(mean_arr_delay = mean(arr_delay, na.rm = TRUE))

##
## exo3

## créez un tableau qui montre 
## le retard au depart par aéroport de départ
## JFK, EWR, LGA
df %>% 
  group_by(origin) %>% 
  summarise(mean_arr_delay = mean(arr_delay, na.rm = TRUE))

## exo4

## what is the AVERAGE *speed* (miles/minute) of 
## flights departing *around midday (11 - 13)*
## by *month*

exo4 <- df %>% 
  filter(dep_time >= 1100 & dep_time <= 1300) %>% 
  mutate(speed = distance/air_time) %>% 
  group_by(month) %>% 
  summarise(mean_speed = mean(speed, na.rm = T))

## exo 5
## mean delay of flights by carrier, for each month

df %>% 
  group_by(carrier, month) %>% 
  summarise(mean_delay = mean(dep_delay, na.rm = T)) %>% 
  arrange(mean_delay)

## exo 6
## maximum departure delay occurred 
## for each of the three NYC airports
## by each day

df %>% 
  group_by(month, day, origin) %>% 
  summarise(max_delay = max(dep_delay, na.rm = T)) %>% 
  arrange(-max_delay)

## exo 7 
## mean air time for each of these three airports? 
## from which airport do the longer haul flights depart?

df %>% 
  group_by(origin) %>% 
  summarise(mean_air_time = mean(air_time, na.rm = T), 
            sd_air_time = sd(air_time, na.rm = T)) %>% 
  arrange(-mean_air_time)

library(tidyverse)
df <- ("read_tsv(Data/plotme.tsv")
## calculer la moyenne,lader st,de x et y par "dataset"
df %>%
group_by

##on attaque avec ggpt2
##un jeu de données pour faire nos essais
mpg