
library(tidyverse)

#creation de vecteur
vect <- c(1,2,3,4,5)

vectnom <- c("la","re","si","do")

c(1,5,7,6,8,10) -> vectinverse

#matrice
vectG <- c(1,5,4,3)
vectH <- c(3,4,5,1)

matrice1 <- rbind(vectG,vectH)
matrice2 <- cbind(vectG,vectH)

matrice1[1,2]

library(tidyverse)
df <- mpg

mpg$year -> mpg_year_extrait

#'chr' chaine de caractere
#'dbl' c'est un numero réel
#'int' numero entier

is.character(mpg$year)
is.integer(mpg$year)

as.double(mpg$year)
as.character(mpg$year)

#exo

vect1 <- c(1,2,1,2)
vect2 <- c(4,5,3,4)
cbind(vect1,vect2) -> matrice

#exo

