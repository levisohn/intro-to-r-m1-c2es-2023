##Notes du cours 3 - Graphiques

library(tidyverse)

df <- read_tsv("Data/plotme.tsv")

#calcul de la moyenne,sd, de x et y par "dataset"

df %>% 
  group_by(dataset) %>% 
  summarise(mean_x = mean(x), mean_y = mean(y),
            devst_x = sd(x), devst_y = sd(y))

#plot
df %>% 
  ggplot()+
  aes(x, y)+
  geom_point()+
  facet_wrap(~dataset)

#ggplot2
#jeu de données pour des essais
mpg

#un plot commence tjrs avec ggplot
ggplot(mpg)

#on utilise l'opérateur "+"
plot1 <-ggplot(mpg)+  #données
  aes(x = cty, y = hwy)+ #ce qu'on met en x et y

plot1

plot1+
  geom_point() 

plot1 +
  geom_smooth()+
  
plot1 +
  geom_point()+
  geom_smooth()

#ajouter autres couches

plot1+
  geom_point(aes(color = displ))

plot1+
  geom_point(aes(size = displ, color = cyl , shape = fl))


plot <-ggplot(mpg)+  
  aes(x = cty, y = hwy)+
  geom_point(aes(size = displ, color = cyl , shape = fl))

plot

##faceting

plot+
  geom_point()+
  facet_wrap(~class)

##ex1
library(nycflights13)

#plotter la relation entre dep_delay et arr_delay, pour le mois de janvier
#faire un subplot pour chaque aéroport

flights %>% 
  filter(month==1) %>% 
  ggplot()+
  aes(x = dep_delay, y = arr_delay, color = origin)+
  geom_point()+
  facet_wrap(~origin)

##ex2
#calculer le delai moyen au départ par origine et destination
#sauvegarder dans le jeu de données ex2 et plotter ce delai moyen par origine et destination

ex2 <- flights %>% 
  group_by(origin, dest) %>% 
  summarise(mean_delay = mean(dep_delay, na.rm =TRUE))

ex2 %>% 
  ggplot()+
  aes(x = dest, y = mean_delay, color = mean_delay)+ #assignation des variables aux axes
  geom_point()+ #on fait des points
  facet_wrap(~origin) #plusieurs facet (tables)

##plot d'une variable 
#var discrète

p <- ggplot(mpg)+aes(x = drv)

p + geom_bar()  

#possibilité de modifier élém géométriques
#couleur "color"
#remplissage "fill"

#avec aes = mapper une variable (ou plus)
p + geom_bar(aes(fill = drv))

#sans aes = changer pour tt
p + geom_bar(fill = 'pink')

#possibilité mapper couleur sur autre variable
p + geom_bar(aes(fill = class))

#une variable continue
p2 <- ggplot(mpg)+aes(x = hwy)
p2

#histogram
p2 + geom_histogram()

#manipuler nb de bins
p2 + geom_histogram(bins = 1)
p2 + geom_histogram(bins = 5)

p2 + geom_histogram() + 
  facet_wrap(~drv)

#densité
p2 + geom_density()

#manipuler le kernel density estimation
p2 + geom_density(adjust = 0.1)
p2 + geom_density(adjust = 10)

#avec le reste
p2 + geom_density(aes(color = drv))
p2 + geom_density(aes(fill = drv))+
  facet_wrap(~drv)

##avec 2 variables
#2 continues

p3 <- ggplot(mpg)+aes(x = cty, y = hwy)
#scatter
p3 + geom_point()
#smooth
p3 + geom_smooth()

#smooth linéaire
p3 + geom_smooth(method = "lm")+geom_point()

p3 + geom_smooth(aes(color = drv), method = "lm")

#une variable continue discrète
p4 <- ggplot(mpg)+aes(x=drv, y=hwy)
p4                   

#scatter
p4 + geom_point()
  
#boxplot
p4 + geom_boxplot()

#violons
p4 + geom_violin() #=geom_density, mais par niveaux

#des barres
#avec la fonction geom_col() avec 2 variables
mpg %>% 
  group_by(drv) %>% 
  tally() %>% 
  ggplot() +
  aes(drv, n) +
  geom_col()

##2 variables catégorielles
mpg %>% 
  ggplot()+
  aes(drv, class)+
  geom_count()

##3 variables
#geom_tile()
ggplot(mpg)+
  aes(x=cyl, y=drv, fill = hwy)+
  geom_tile()

