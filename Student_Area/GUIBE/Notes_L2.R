#installation d'un package
install.packages("nycflights13")

#pour utiliser le package, il faut l'appeler en utilisant "library"
library(tidyverse)
library(nycflights13)

#importer les données dans l'espace de travail
df <- flights

#voir les données dans console
df

#voir données dans R
View(df)

#fonction sommaire des données
summary(df)

#autre fonction pour jeu de données
install.packages("skimr")
library(skimr)
skim(df)

###manipulation données
##filter = extrait lignes filter(data, la logique)
#tous les vols de janvier
filter(df, month == 1)

#plusieurs filters same time
#vols du 12 janvier
filter(df, month == 1 & day == 12)

#logique négative
#les vols qui ne sont pas partis de JFK
filter(df, origin != "JFK")

##multiple statesments avec %in%
#vol du printemps (= mars, avril mai)
printemps <- filter(df, month == 3| month == 4 | month == 5)
filter(df, month %in% c(3, 4, 5))

##mise en ordre
#arrange()
arrange(df, dep_delay)

#ordre croissant
arrange(df, dep_delay)
#ordre décroissant
arrange(df, -dep_delay)

##selectionner colonnes : select()
#df avec date, lieu départ, lieu arrivée
select(df, year, month, day, origin, dest)
#ou bien
select(df, -tailnum)

#using starts_with
#toutes les variables relatives au "delay"
names(df)
select(df, starts_with("dep"))

#contains()
select(df, contains("dep"))

##rename = changer noms variables
#passer en français

df2 <- rename(df, mois = month,
           année = year,
           jour = day)

df2


##exercice 1
#créer jeu de données "noel" contenant tout les vols du 25/12 avec variables : dep_time et dep_delay
noel <- filter(df,month == 12 & day == 25)
noel <- select(noel, starts_with("dep"))

###dire bonjour au "pipe" %>% (ctrl + shift + M)= "et après/then"
filter(df, month == 12 & day == 25)

##deux codes équivalents

df %>% 
  filter(month == 12 & day == 25) %>% 
  select(starts_with("dep"))

#exemple avec des maths

2*2
exp(2*2)

(2*2) %>% 
  exp()

##exercice 2
#créer jeu de données contenant tous les vols entre JFK et LAX partis en mars et garderle jour et le retard, puis ordonner par retard décroissant (et rename en français)
exo2 <- df %>% 
  filter(origin == "JFK" & dest == "LAX") %>% 
  filter(month == 3) %>% 
  select(day, dep_delay) %>% 
  arrange(-dep_delay) %>% 
  rename(jour = day, retard = dep_delay) 

###changing data
##with mutate()
#create an index starting at 1 for 2013
df %>% 
  mutate(starting_year = year - 2012)

#calculer vittesse avions
#distance parcourue est "distance" en miles
#temps en l'air est "air_time" en minutes
df %>% 
  mutate(speed_basic = distance/air_time) %>% 
  select(distance, air_time, speed_basic)

#on peut utiliser les nouvelles variables
#créer la vitesse et la vitesse au carré

#avec deux calls to mutate
df %>% 
  mutate(speed_basic = distance/air_time) %>% 
  mutate(speed2 = speed_basic^2) %>% 
  select(distance, air_time, speed_basic, speed2)

#avec le même call
df %>% 
  mutate(speed_basic = distance/air_time, speed2 = speed_basic^2) %>% 
  select(distance, air_time, speed_basic, speed2)

#create a logical variable
#une variable qui me dit si l'aéroport est JFK
df %>% 
  mutate(is_JFK = origin == "JFK") %>% 
  select(origin, is_JFK)

#attention, si mutate est assigné à une nouvelle variable, ok
#           si mutate est assigné à la même variable, on l'écrase

df %>% 
  mutate(origin2 = "abc") %>% 
  select(origin, origin2)

#summarise pour faire des sommaires de variable
#une moyenne
#quel est le delay moyen au départ
df %>% 
  summarise(mean_delay = mean(dep_delay, na.rm = TRUE), #na.rm = TRUE pour enlever les obs manquantes "NA"
                         mean_arr_delay = mean(arr_delay, na.rm =TRUE))

##faire des sommaires groupés avec group_by()

#quel est le retard moyen à l'arrivée pour chaque mois
df %>% 
  group_by(month) %>% 
  summarise(mean_arr_delay = mean(arr_delay, na.rm =TRUE))

#on peut grouper par plus qu'une variable
#retard par mois et aéroport de départ
df %>% 
  group_by(origin, month) %>% 
  summarise(mean_arr_delay = mean(arr_delay, na.rm =TRUE))

###exercice 3
##retard au départ par aéroport de départ
##JFK, EWR, LGA

df %>% 
  group_by(origin) %>% 
  summarise(mean_arr_delay = mean(arr_delay, na.rm =TRUE))

##exercice 4
#quelle est la vitesse moyenne de vol autour de 11h et 13h par mois

exo4 <- df %>% 
  filter(dep_time >= 1100 & dep_time <= 1300) %>% 
  mutate(speed = distance/air_time) %>% 
  group_by(month) %>% 
  summarise(mean_speed = mean(speed, na.rm = TRUE))

#exercice 5
#quel est le delai moyen des vols par compagnie pour chaque mois ?

exo5 <- df %>% 
  group_by(carrier, month) %>% 
  summarise(mean_delay = mean(dep_delay, na.rm =TRUE)) %>% 
  arrange(mean_delay)

#exo 6
#maximum departure delay for each of there NYC airports by each day

df %>% 
  group_by(month, day, origin) %>% 
  summarise(max_delay = mean(dep_delay, na.rm =TRUE)) %>% 
  arrange(-max_delay)

#exo 7

df %>%
  group_by(origin) %>% 
  summarise(mean_air_time = mean(air_time, na.rm = TRUE)) %>% 
  arrange(-mean_air_time)
  
  
  
  
  







