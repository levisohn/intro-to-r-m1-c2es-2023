library(tidyverse)
library(nycflights13)

#on utilise 3 jeux de données
planes <- planes
flights <- flights
airports <- airports

flights
planes
airports

## check si key est unique

planes %>% 
  group_by(tailnum) %>% 
  tally() %>% 
  filter(n>1)

## est ce qu'il y a une relation entre l'age des avions et la distance parcourue ? 

df_left<-flights %>% 
  select(tailnum,distance)

df_right<-planes %>% 
  select(tailnum,year)

exemple1<-df_left %>% 
  left_join(df_right, by="tailnum")

#réponse à la question de façon graphique

exemple1 %>% 
  ggplot()+
  aes(year,distance)+
  geom_point()+
  geom_smooth()

##reshape
##pivot_longer
table4a
# deux pb, les données sont cachées dans le nom des variables
#données à l'intérieur du tableau n'ont pas un nom de variable (on ne sait pas)

#on va transformer le jeu en plus long

table4a %>% 
  pivot_longer(cols=-country,
               names_to="year",
               values_to="cases")
##pivot wider

table2
#deux pb, chaque observation est sur deux lignes
#la variable count contient de l'info qui n'est pas uniforme

#on va transformer le jeu de données en "plus large"

table2 %>% 
  pivot_wider(names_from = type,
              values_from = count)


## pivot_longer et pivot_wider sont des opérations inverses

table1 

table1 %>% 
  pivot_longer(cols=!contry & !year,
               names_to = "names",
               values_to="values") %>% 
  pivot_wider(names_from=names,values_from=values)

#exercice
#créer un df de personnes qui s'appelle "Mary"
#isoler les variables year, sex, name, prop
#le df est long, transformer en wide avec une variable pour chaque genre

babynames %>% 
  filter (name=="Mary") %>% 
  select(-n) %>% 
  pivot_wider(names_from = sex,values_from=prop)

##utiliser fonction pivot
#1-pour nettoyer données
#quand on commence le travail on met en forme pour mieux travailler
#2-pour présenter nos résultats, on fait des analyses et on veut avoir un tableau
#joli et facile à lire

## exercice sur utilité 2
#calculer le retard à l'arrivée moyen par aeroport de départ et destination
#créer un tableau où les aeroports de départ ont chacun leur colonne

library(nycflights13)
flights

flights %>% 
  group_by(origin, dest) %>% 
  summarize(mean_arr_delay=mean(arr_delay, na.rm=TRUE)) %>% 
              pivot_wider(names_from=origin, values_from=mean_arr_delay)
##point2, prend nom de origin, sur des colonnes  différentes  




#calculer le retard moyen par mois et par aeroport d'origine
#créer un tableau où les mois ont chacun leur colonne

flights %>% 
  group_by(origin, month) %>% 
  summarize(mean_dep_delay=mean(dep_delay, na.rm=TRUE)) %>% 
  pivot_wider(names_from=month, values_from=mean_dep_delay)

##separate and unite
#what if info is hidden in a variable?

table3
#problème
#"rate" contient les cas+ la population

#on utilise "separate"

table3 %>% 
  separate(col=rate, into=c("casi","populazione"),
           sep="/")

##quels séparateurs sont possibles ?
#on sépare en trois variables, au caractère 3 et 6 
table3
table3 %>% 
  separate(col=rate,into=c("A","B","C"),
           sep=c(3,9))

##be aware of the variable type
#solution avec convert
table3 %>% 
  separate(col=rate, into=c("cast","populazione"),
           sep="/", convert=TRUE)

#solution 2, parfois c'est bien de lister toutes les opérations

table3 %>% 
separate(col=rate, into=c("casi","populazione"),
         sep="/") %>% 
  mutate(cast=as.numeric(casi),
         populazion=as.numeric(populazione))


##inverse de separate et unite

table5

table5 %>% 
  unite(col="année",century,year, sep="")

#re séparer ce qu'on a unit, faut pas mettre "" sinon trop compiqué, avec "_"
table5 %>% 
  unite(col="année",century,year, rate) %>% 
  separate(col="année",
           into=c("country","year","century","rate"),
           sep="_")
