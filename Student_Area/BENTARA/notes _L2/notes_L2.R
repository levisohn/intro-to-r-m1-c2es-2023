install.packages("nycflights13")

#quand on veut utiliser un package il faut l'appeler en utilisant library 
library(tidyverse)
library(nycflights13)

#importer les données dans l'espace de travail
df <- flights

#voir les données dans la console
df

#voir données dans Rstudio 
View(df)

#fonction sommaire des données
summary(df)

#autre fonction oire prise en main jeu données
install.packages(skimr)
library(skimr)
skim(df)

#data manipulation
#filter
filter(df,month==1 & day==12)
filter(df,origin != "JFK")

#multiple statement avec %in%
#printemps "mars,avril,mai
filter (df, month == 3| month == 4| month ==5)
filter (df, month %in% c(3,4,5))

#mise en ordre des données
#croissant
arrange(df, dep_delay)
#decroissant
arrange (df, -dep_delay)

#selectionner des colonnes
select (df, year, month, day, origin, dest)

#eliminer des variables
select (df, -tailnum, - month, -year, -dest)

#toutes les variables relatives au "delay"
names (df)
select (df, ends_with("delay"))

select(df, starts_with("dep"))

select (df, contains("dep"))

select (df, month, year, day, contains ("dep"))

# renam changer nom de variable
# passer tout en francais
df2 <- rename (df, mois=month,
                  annee=year,
                  jour=day)

df

#exo 1
# créer jeu de donnée qui s'appelle noel et contient tous vols 25 decembre avec variable dep_time et dep_delay

noel <- filter(df, month==12 & day == 25)
noel <- select(noel,starts_with("dep")) 

#dites bonjour au "pipe" (ctrl+shift+m)

df %>% 
  filter (month==12 & day == 25) %>% 
  select (starts_with("dep")) 

#exemple avec des maths
2*2
exp(2*2)

2*2 %>% 
  exp()

#en utilisant pipe, créer base données qui contient tous vols entre JFK LAX partis en mars avec jour et 
#retard , décroissant, mettre nom en francais

df %>% 
  filter (month==3, origin=="JFK", dest=="LAX") %>% 
  select (day, dep_delay) %>% 
  arrange (-dep_delay) %>% 
  rename ( jour=day, retard=dep_delay)

#créer nouvelle variable ou changer une existante

df %>% 
  mutate(starting_year = year - 2012)

#calculer vitesse des avions et vitesse au carré
#distance (en MILES) temps (MINUTES)
df %>% 
  mutate (speed_basic = distance/air_time) %>% 
  mutate (speed2 = speed_basic^2) %>% 
  select (distance, air_time, speed_basic, speed2)

#créer une variable logique, variable qui me dit si l'aeroport est JKK
df %>% 
  mutate(is_JFK=origin=="JFK") %>% 
  select (origin, is_JFK)

# ATTENTION, si mutate est assigné à une nouvelle variable, tout va bien
#si mutate est assigné à la même variable, on l'ecrase

df %>% 
  mutate( origin2="paolo") %>% 
  select (origin,origin2)

#quel est le delay moyen au départ
df %>% 
  summarize(mean_delay=mean(dep_delay, na.rm=TRUE),
            mean_arr_delay=mean(arr_delay, na.rm=TRUE))
#na.rm pour enlever les valeurs manquantes

#summarize groupé
#quel est le retard moyen à l'arrivé pour chaque mois

df %>% 
  group_by(month) %>% 
  summarize(mean_arr_delay=mean(arr_delay,na.rm=TRUE))
#on peut gouper par plus qu'une variable

#par mois et aeroport de départ
df %>% 
  group_by(origin, month) %>% 
  summarize(mean_arr_delay=mean(arr_delay,na.rm=TRUE))

#créer tableau qui montre le retard départ aeroport départ JFK EWR LGA
df %>% 
  group_by(origin) %>% 
  summarize(mean_arr_delay=mean(arr_delay,na.rm=TRUE))

#vitesse moyenne vols qui partent de 11 a 13H par mois

exo4 <- df %>% 
  filter(dep_time >= 1100 & dep_time <= 1300) %>% 
  mutate (speed3 = distance/air_time) %>%
  group_by(month) %>% 
  summarise (mean_speed=mean(speed3,na.rm=TRUE))

#EXERCICES
df %>% 
  group_by(carrier, month, year) %>% 
  summarise (mean_delay=mean(dep_delay,na.rm=TRUE)) %>% 
  arrange(mean_delay)
              

df %>% 
  group_by(day,month,origin) %>% 
  summarize(max_delay=max(dep_delay,na.rm=TRUE)) %>% 
  arrange (-max_delay)

df %>% 
  group_by(origin,month) %>% 
  summarize(mean_air_time=mean(air_time,na.rm=TRUE)) %>% 
  arrange(-mean_air_time)

