## L4

library(tidyverse)
library(nycflights13)

planes <- planes
flights <- flights
airports <- airports

flights


## ici on compte, il faut trouver une clé
planes %>% 
  group_by(tailnum) %>% 
  tally() %>% 
  filter(n>1)

df_left <- flights %>% 
  select(tailnum, distance)

df_right <- planes %>% 
  select(tailnum, year)

exemple1 <- df_left %>% 
  left_join(df_right, by = "tailnum")

exemple1 %>%
  ggplot()+
  aes(year, distance)+
  geom_point()+
  geom_smooth()



## reshape
table4a %>% 
  pivot_longer(cols = -country, names_to = "year", values_to = "cases")

table2 %>% 
  pivot_wider(names_from = type, values_from = count)


babynames

babynames %>% 
  filter(name == "Mary") %>% 
  select(-n) %>% 
  pivot_wider(names_from = sex, values_from = prop)


flights %>% 
  group_by(origin, dest) %>% 
  summarise(mean_arr_delay = mean(arr_delay, na.rm = T)) %>% 
  #
  pivot_wider(names_from = origin, values_from = mean_arr_delay)

flights %>% 
  group_by(month, origin) %>% 
  summarise(mean_arr_delay = mean(arr_delay, na.rm = T))


table3 %>% 
  separate(col = rate, into = c("case", "population"),
           sep="/")

table3 %>% 
  separate(col = rate, into = c("A", "B", "C"),
           sep = c(3,2))

table5 %>% 
  unite(col = "année", century, year)
