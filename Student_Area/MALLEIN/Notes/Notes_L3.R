## cours sur les graphiques

library(tidyverse)

df <- read_tsv("Data/plotme.tsv")

## moyenne, la dev, de x et y par "dataset"
df %>% 
  group_by(dataset) %>% 
  summarise(mean_x = mean(x), mean_y = mean(y),
            devst_x = sd(x), devst_y = sd(y))

## plot 

## pas graphique en 3D, pas de camenbert

## mpg = mass per garden
mpg

## operateur "+" (on ajoute des couches)
# on assigne les variables à x et y
# il faut voir ça comme une phrase avec pronom verbe sujet
firstplot <- ggplot(mpg)+
  aes(x = cty, y = hwy)

## avec geom, on a plein de type de graphiques
firstplot +
    geom_smooth()

## avec geom, on a plein de type de graphiques
firstplot +
  geom_point(aes(color = class))

## ici 3 variables = x, y, couleur
firstplot +
  geom_point(aes(color = class))

## facet 
firstplot +
  geom_point()+
  facet_wrap(~class)

## exo1
library(nycflights13)
## plotter la relation entre dep_delay et arr_delay
## pour le mois de janvier

#correction par le prof
flights %>% 
  filter(month==1) %>% 
  ggplot()+
  aes(x = dep_delay, y = arr_delay)+
  geom_point()+
  facet_wrap(origin)
## ca marche pas de mon coté

##exo2
## Calculer le delais moyen au depart par origine et destination
## sauvegarder ça dans le jeu de données "exo2"
## plottez ce delai moyen par origine et destination
exo2 <- flights %>% 
  group_by(origin, dest) %>% 
  summarise(mean_delay = mean(dep_delay, na.rm = T))

## (na.rm = ne toccupe pas des valeurs manquantes)

exo2 %>% 
  ggplot()+
  aes(y = dest, x = mean_delay)+
  geom_point()+
  facet_wrap(-origin)
## ca marche pas de mon coté, il ne faut pas mettre un tiret mais une vague

## var discrète
p <- ggplot(mpg)+aes(x=drv)

p + geom_bar(aes(fill =drv))

## on ajoute de la couleur

p2 <- ggplot(mpg)+aes(x=hwy)

## histogram
p2 + geom_histogram(bins = 1000)

## 2 variables

p3 <- ggplot(mpg)

## blblabla

## 3 variables

ggplot(mpg)+
  aes(x = cyl, y = drv, fill = hwy)+
  geom_title()

## aller sur "help" puis sheet... pour avoir des astuces en images









            