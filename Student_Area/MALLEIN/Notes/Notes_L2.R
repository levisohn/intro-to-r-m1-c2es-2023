## COURS 2 - 07/09/2023

## POUR INSTALLER UN PACKAGE QUI CONTIENT DES DONNEES
install.packages("nycflights13")

## POUR UTILISER LE PACKAGE IL FAUT L'APPELER EN FAISANT UN LIBRARY TIDYVERSE
## PUIS LAUTRE LIBRARY

library(tidyverse)
library(nycflights13)

## importer les données dans l'espace de travail
df <- flights

## voir les données dans la console
df

## voir les données dans Rstudio ou simplement cliquer dans environment
view(df)

## fonction de sommaire des données
summary(df)

## autre fonction pour prise en main de données
install.packages("skimr")
library(skimr)

## permet de vérifier si les données sont bonnes et qu'il n'y a pas d'erreur
skim(df)

## filter

## TOUS LES MOIS DE JANVIER
filter(df, month == 1)

## utiliser PLUSIUERS FILTRES
filter(df, month == 1 & day == 12)

## utiliser la negation
filter(df, origin != "JFK")

## multiple statment avec %in%

printemps <- filter(df, month == 3 | month == 4 | month == 5)

## exemple, ça permet de raccourcir 
filter(df, month %in% c(3, 4, 5))


## mise en ordre des données
## arrange()

## ordre croissant
arrange(df, dep_delay)

## ordre decroissant
arrange(df, -dep_delay)

## sélctionner des collones: select()
select(df, year, month, day)

## si trop de variables, on peut en supprimer
select(df, -nom de la variable)

## voir toutes les variables
names(df)

select(df, ends_with("delay"))

## rename
## pour changer le nom des varia
## on le fait pas souvent sauf si c'est tres long

rename(df, mois = month, 
       annee = year,
       jour = day)

### exo 1
## Créer un jeu de données qui s'appelle "noel"
## et qui contient tous les vols du 25 décembre
## avec variables dep_time et dep_delay

## ma réponse
noel <- filter(df, month == 12, day == 25, dep_times<)

## correction
noel <- filter(df, month == 12 & day == 25)
noel <- select(noel, starts_with("dep"))

## "%>%" --> ctr+shift+M qui signifie "et après/puis/then"

## c'est un equivalent de ce qu'on a fait avant mais plus pratique à utiliser

## exo2
## tous les vols entre JFK et LAX
## partis en mars

df %>% 
  filter(origin == JFK & LAX)

## correction 
exo2 <- df %>% 
  filter(origin == "JFK" & "LAX") %>% 
  filter(month == 3) %>%
  select(day, dep_delay) %>% 
  arrange(-dep_delay) %>% 
  rename(jour = day, retard = dep_delay)


## Uitilisation de "mutate"

## utilisation de summarise

## group_by() -> la fonction la plus utile

## exo 3
## 

df %>% 
  group_by(origin) %>% 
  summarise(mean_arr_delay = mean(arr_delay, na.rm = TRUE))

## ex4 : filter, créer, summurise
## correction
df %>% 
  filter(dep_time > 1100 & dep_time < 1300) %>% 
  mutate(speed = distance/air_time) %>% 
  group_by(month) %>% 
  summarise(mean_speed = mean(speed, na.rm = T))

## exos : use the pipe

df %>%
  group_by(carrier, month) %>% 
  summarise(mean_delay = mean(dep_delay, na.rm = T)) %>% 
  arrange(mean_delay)

## exos 6
df %>% 
  group_by(month, day, origin) %>% 
  summarise(max_delay = max(dep_delay, na.rm = T)) %>% 
  arrange(-max_delay)

## exo7
df %>% 
  group_by(origin) %>% 
  summarise(mean_air_time = mean(air_time, na.rm = T)) %>% 
  arrange(-mean_air_time)


















