---
title: "Introduction to R and the tidyverse"
subtitle: "-- data wrangling part 1 --"
author: "Paolo Crosetto"
format:
  revealjs: 
    theme: dark
---

```{r setup, include=FALSE, warning=FALSE, message=FALSE, fig.align='center'}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
theme_set(theme_minimal())
```

## Todays topics

> today we will deal with **four** topics:

1.  data import and export
2.  manipulating data
3.  joining data from different tables
4.  *tidying* data

## Before we start: nycflights

```{r}
#install.packages("nycflights13")
```

-   some data about **all** flights from New York airports in 2013
-   we get to know arrival & departure times, delays, carrier...
-   tail number, origin, destination...
-   not particularly interesting *per se* but big (336K observations)

# 1. Importing data

## getting data into R: packages

> up to now we have worked with data from *packages* (`mpg`; `nycflights`)

-   easy to do: install a package, then call a dataset
-   all the hard work has been made for you
-   if you wish you can import the data into your workspace

```{r}
library(nycflights13)
df <- flights
```

## getting data into R: other sources

> life is not always that easy. You might have...

-   ...data in the form of (aaarg!) Excel files
-   ...comma separated (`.csv`) data
-   ...data coming from SPSS, SAS, STATA, ...
-   or text data from ASCII sources

```{r, echo=FALSE, results='hide'}
rm(df)
```

## getting data into R: `readr`

-   when you load the tidyverse you automatically load `readr`
-   this is a package that gives you functions to load data

`read_csv("filename")`

-   of the form `read_...` with different file types

## A simple example

> you find some data here: <https://goo.gl/kPycfH>

-   this is the `human development index`, by country

-   highest numbers (nearest to 1) are better

-   save the file to disk in your own folder

-   save it as `HDI.csv`

-   open it up with a text editor: what do you see?

## A simple example

> now that your data is saved, how do you import it to R?

-   you use `read_csv("path_to_file")`
-   in your case, it should be "`/Student_Area/SURNAME/HDIdata.csv`"
-   in my case:

```{r}
df <- read_csv("/home/paolo/Dropbox/Public/HDIdata.csv")
```

## There is more than .csv

> `read_csv` just made many things for you

-   automatically detect names
-   automatically detect column types
-   ...

::: fragment
-   other useful functions:
    -   if the separator is `;` rather than `,` use `read_csv2`
    -   if the separator is a `TAB` rather than `,` use `read_tsv`
:::

## Some hints

-   you can always export to `.csv` in all programs

-   *even in Excel!*

-   so once you have exported to `.csv`, all is fine

-   other binary formats (`.dta`, `.xlsx`) force you to have the right tool

> keep a copy of your data in a **text-based format**

# 2. Data inspection

## Getting to know the data: inspection

```{r}
library(nycflights13)

flights
```

## Getting to know the data: View

```{r}
View(flights)
```

-   `View()` opens an Rstudio data window
-   in that window you can
-   sort
-   arrange
-   inspect variables

## Inspecting data and summary statistics

```{r}
summary(flights)
```

## Importing data in your workspace

-   `flights` is not yet in your workspace
-   to import it, assign to an object using `<-`
-   we will use the shorthand `df` (you can use any other)

```{r}
df <- flights
df
```

## Inspecting with `skimr`

> There are other packages to inspect data

```{r}
# install.packages(skimr)
library(skimr)
skim(df)
```

# 3. Data manipulation

## `dplyr`

> we will use the package `dplyr`, from the tidyverse

-   don't worry about the strange name, it has a reason.
-   it is called when running `library(tidyverse)`

## Structure of **dplyr**

> simple, direct *verbs* to do the main jobs for you

![](fig/dplyr.png){fig-align="center"}

-   they do **not** alter the data saved on memory

-   which is **good**: `safe` manipulation!

-   if you want to save the altered dataset, use assign `<-`

## Data manipulation: `filter()`

> `filter()` allows to extract **rows** from the data frame

-   `filter(data, logic)`
-   `data` is your data
-   `logic` is a logical statement that tells `filter()` what must be included or not
-   R understands `>, >=, <, <=, !=, ==`
-   R understand also `&, |, !`

## Filtering

> all December flights

```{r}
filter(df, month == 12)
```

## Filtering over multiple criteria

> all Christmas flights departed around midday

```{r}
filter(df, month == 12 & day == 25 & dep_time>1100 & dep_time <1300)
```

## Multiple ***or*** statements: `%in%`

> mutliple `or` statement can be tricky

```{r}
#| eval: false
# you want all summer flights (June, July, August)
filter(df, month == 6 | month == 7 | month == 8)
```

> can become cumbersome!

```{r}
#| eval: false
# you can use the alternative %in%
filter(df, month %in% c(6,7,8))
```

## Sort data: `arrange()`

> `arrange()` lets you sort data

-   it is like the sorting you do in `View()`, but:
    1.  it is done on the console, and
    2.  it lets you save the new order to a data frame
-   this can be useful when looking for special observations

## Selecting columns: `select()`

> What `filter()` is to rows, `select()` is to columns

-   more than one variable? list all variables by bare name: `select(data, var1, var2, var3, ...)`
-   exclude (drop) some variables? minus sign: `select(data, -var1, -var2, ...)`
-   exploit naming patterns? `starts_with("string")`, `ends_with("string")`, `contains("string")`
-   select all variables? `everything()`

## Renaming variables: `rename()`

-   `rename()` = `select()` but keeps all variables
-   you use it as `rename(data, newname = oldname)`
-   e.g. translating var names to French:

```{r}
rename(df, mois = month, jour = day)
```

# 4. Changing data

## Creating new variables: `mutate()`

> you want to create a *new* variable with some manipulation

-   you can use `mutate(data, newvar = f(oldvar))`
-   where `f()` is some function or manipulation

```{r}
df_new <- mutate(df, starting_year = year - 2012)
select(df_new, starting_year)
```

## `mutate()` properties

-   you can create more variables in one single `mutate()` call

```{r}
df_new <- mutate(df, starting_year = year - 2013, 
                 speed = distance /air_time * 60,
                 speed2 = speed**2)
select(df_new, starting_year, speed, speed2)
```

## `mutate()` properties

-   you can *use* right away the newly created variables!

```{r}
df_new <- mutate(df, speed = distance /air_time * 60, 
                 speed_squared = speed ** 2)
select(df_new, speed, speed_squared)
```

## Functions that work with `mutate()`

-   all *vector* functions: input a vector, output a vector
-   arithmetic operations: `+ - * / ^`, `exp()`, `sqrt()`, `log()`
-   offset: `lag()` to refer to the -1 period. *Useful for increments*
-   cumulative functions: `cumsum()`, etc...
-   logical (see `filter()`)

## Summarise your data: `summarize()`

-   create aggregations and summary of your data
-   `summarize(data, newvar = f(oldvar))`
-   similar to `mutate()` but returns a **scalar (one value)**

```{r}
summarize(df, meandelay = mean(dep_delay, na.rm = TRUE))
```

## `summarize()` properties

-   you can do more than one summary in one go

```{r}
summarize(df, meandelay = mean(dep_delay, na.rm = TRUE), 
          sddelay = sd(dep_delay, na.rm = TRUE), 
          maxdeptime = max(dep_time, na.rm = TRUE))
```

## Empowering `summarize()`: `group_by()`

> `summarize()` is not so useful: returns **one** value...

-   much more powerful if we can **group data**: 1 value/group
-   `group_by()` groups the `df` by grouping variable levels
-   nothing changes; but R knows

```{r}
group_by(df, month)
```

## example: delay by month

-   output has 12 rows

```{r}
df_bymonth <- group_by(df, month)
summarize(df_bymonth, meandelay = mean(dep_delay, na.rm = TRUE))
```

## example: delay by month **and** day

-   output has 365 rows

```{r}
df_bymonth_byday <- group_by(df, month, day)
summarise(df_bymonth_byday, meandelay = mean(dep_delay, na.rm = TRUE))
```

# 5. Putting it all together

## Exercise: a complex example

> **what is the speed of flights departing around midday (11 - 13), by month?**

-   the input is the flights df
-   use `filter()`, `select()`, `mutate()`, `group_by()` and `summarize()`
-   the final output is a df with 2 variables (month and speed) and 12 observations (one for each month)

> *à vous de jouer!*

## Exercise: solution

-   let's combine `filter()`, `select()`, `mutate()`, etc...

-   we want to know the average speed of flights departing around midday, by month

```{r, results=FALSE}
df_midday <- filter(df, dep_time > 1100 & dep_time < 1300)
df_midday_reduced <- select(df_midday, month, air_time, distance)
df_midday_speed <- mutate(df_midday_reduced, 
                          speed = distance / air_time * 60)
df_midday_grouped <- group_by(df_midday_speed, month)
df_midday_final <- summarise(df_midday_grouped, 
                             meanspeed = mean(speed, na.rm = TRUE))
```

```{r}
df_midday_final
```

## Multiple operations: problems

> the code in the previous slide has several problems

-   it forces you to create several different `data frame`s

-   this clutters your environment

-   hard to find proper names

-   easy to make errors

-   `what can we do?`

## Enters the pipe! `%>%`

> the *pipe* operator ("*then") ("et après*")
>
> Insert with `ctrl+shift+M`

![](fig/ceci.jpg){fig-align="center"}

## Enters the pipe! `%>%`

> the *pipe* operator ("*then") ("et après*")

![](fig/dplyr_filter.png){fig-align="center"}

## Pipe as maths

> $f(a, x) \Rightarrow a$ %\>% $f(x)$

```{r}
2*2
exp(4)
(2*2) %>% exp()
```

## Pipe and `dplyr`

-   since *dplyr* verbs always return a data frame, and always have a data frame as first input, this saves your life

    ![](fig/example.webp)

## the same code with the pipe

```{r}
df_midday  %>%  filter(dep_time > 1100 & dep_time < 1300) %>% 
                select(month, air_time, distance) %>% 
                mutate(speed = distance / air_time * 60) %>% 
                group_by(month) %>% 
                summarise(meanspeed = mean(speed, na.rm = TRUE))
```

## Exercises: use the pipe

1.  what is the `mean delay` of flights by `carrier`, for each `month`?
2.  what is the `maximum departure delay` occurred for each of the three NYC `airports`, by each `day`?
3.  what is the `mean air time` for each of these three `airports`? from which airport do the longer haul flights depart?
